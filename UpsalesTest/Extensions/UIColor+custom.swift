//
//  UIColor+custom.swift
//  UpsalesTest
//
//  Created by Darko Jovanovski on 10/30/18.
//  Copyright © 2018 Darko Jovanovski. All rights reserved.
//

import UIKit

extension UIColor {
    
    //MARK: - All Colors
    static let navy = UIColor.color(withRed: 0, green: 32, blue: 84, alpha: 1)
    static let brightBlue = UIColor.color(withRed: 74, green: 144, blue: 226, alpha: 1)
    static let greyish = UIColor.color(withRed: 220, green: 220, blue: 220, alpha: 1)

    
    //MARK: -
    static func color(withRed red: CGFloat,
                      green: CGFloat,
                      blue: CGFloat,
                      alpha: CGFloat) -> UIColor {
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: alpha)
    }
}
