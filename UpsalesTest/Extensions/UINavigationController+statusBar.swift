//
//  UINavigationController+statusBar.swift
//  UpsalesTest
//
//  Created by Darko Jovanovski on 10/31/18.
//  Copyright © 2018 Darko Jovanovski. All rights reserved.
//

import UIKit

extension UINavigationController {
    override open var childForStatusBarStyle: UIViewController? {
        return self.topViewController
    }
}
