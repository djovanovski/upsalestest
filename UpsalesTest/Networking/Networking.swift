//
//  Networking.swift
//  UpsalesTest
//
//  Created by Darko Jovanovski on 10/30/18.
//  Copyright © 2018 Darko Jovanovski. All rights reserved.
//

import UIKit

class Networking: NSObject {
    
    /**
     Small class for network request
     
     - Usualy i devide this in multiple classes i have my own restmanager with protocol and separate rest requests.
     - I prefer to use Alamofire but I also enjoyed using Codable :)
     - Needs big refactoring, but for the time being this is it...
     */
    
    
    let baseUrl = "https://integration.upsales.com/api/v2/"
    let token = "?token=a23e10aa5cd0641b6b63209f3c2e44562784c859777527d331bfccf14553b044"
    let isExternal = "&isExternal=0"
    let sort = "&sort=name"
    let limit = "&limit=11"
    let accountsEndpoint = "accounts/"

    func getAccounts(offset: Int, completion: @escaping (Result<Companies>) -> Void) {
        
        let limitOffset = "&offset=\(offset)&limit=11"

        let endPoint = "\(baseUrl)" + "\(accountsEndpoint)" + "\(token)" + "\(limitOffset)" + "\(sort)" + "\(isExternal)"
        
        guard let url = URL(string: endPoint) else { return completion(.Error("Invalid URL, we can't update your feed")) }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            guard error == nil else { return completion(.Error(error!.localizedDescription)) }
            guard let data = data else { return completion(.Error(error?.localizedDescription ?? "ERROR"))
            }
            do {
                let myStruct = try JSONDecoder().decode(Companies.self, from: data) // do your decoding here
                completion(.Success(myStruct))
            } catch let error {
                return completion(.Error(error.localizedDescription))
            }
            }.resume()
    }
    
    func getUsers(completion: @escaping (Result<AccountManagers>) -> Void) {
        
        let usersEndpoint = "users/"

        let endPoint = "\(baseUrl)" + "\(usersEndpoint)" + "\(token)"
        
        guard let url = URL(string: endPoint) else { return completion(.Error("Invalid URL, we can't update your feed")) }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            guard error == nil else { return completion(.Error(error!.localizedDescription)) }
            guard let data = data else { return completion(.Error(error?.localizedDescription ?? "ERROR"))
            }
            do {
                let myStruct = try JSONDecoder().decode(AccountManagers.self, from: data) // do your decoding here
                completion(.Success(myStruct))
            } catch let error {
                return completion(.Error(error.localizedDescription))
            }
            }.resume()
    }
    
    func getAccounts(withUserIds: String, completion: @escaping (Result<Companies>) -> Void) {
        
        let endPoint = "\(baseUrl)" + "\(accountsEndpoint)" + "\(token)" + "\(limit)" + "\(withUserIds)" + "\(isExternal)"
        
        guard let url = URL(string: endPoint) else { return completion(.Error("Invalid URL, we can't update your feed")) }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            guard error == nil else { return completion(.Error(error!.localizedDescription)) }
            guard let data = data else { return completion(.Error(error?.localizedDescription ?? "ERROR"))
            }
            do {
                let myStruct = try JSONDecoder().decode(Companies.self, from: data) // do your decoding here
                completion(.Success(myStruct))
            } catch let error {
                return completion(.Error(error.localizedDescription))
            }
            }.resume()
    }
}

enum Result<T> {
    case Success(T)
    case Error(String)
}

