//
//  CompanyHeaderView.swift
//  UpsalesTest
//
//  Created by Darko Jovanovski on 10/30/18.
//  Copyright © 2018 Darko Jovanovski. All rights reserved.
//

import UIKit

class CompanyHeaderView: LoadViewFromXibParentView {
    
    @IBOutlet weak var labelCompanyTitleLabel: UILabel!
    @IBOutlet weak var labelNumberOfCompanies: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .navy
    }
    
    convenience init(frame: CGRect, title: String, companiesNumber: String) {
        self.init(frame: frame)
        self.labelCompanyTitleLabel.text = title
        self.labelNumberOfCompanies.text = companiesNumber
    }
    

}
