//
//  FiltersMock.swift
//  UpsalesTest
//
//  Created by Darko Jovanovski on 10/30/18.
//  Copyright © 2018 Darko Jovanovski. All rights reserved.
//

import UIKit

struct FiltersMock {
    let name: String
    
    init(name: String) {
        self.name = name
    }
}
