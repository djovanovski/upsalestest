//
//  Users.swift
//  UpsalesTest
//
//  Created by Darko Jovanovski on 10/30/18.
//  Copyright © 2018 Darko Jovanovski. All rights reserved.
//

import UIKit

struct AccountManagers: Codable {
    
    let data: [ManagerUsers]
    enum CodingKeys: String, CodingKey {
        case data = "data"
    }
}

struct ManagerUsers: Codable {
    let id: Int
    let name: String
    let email: String
    
    var isSelected: Bool? = false
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case email
    }
    
    
}
