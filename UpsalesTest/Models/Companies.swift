//
//  Model.swift
//  UpsalesTest
//
//  Created by Darko Jovanovski on 10/30/18.
//  Copyright © 2018 Darko Jovanovski. All rights reserved.
//

import Foundation

struct Companies: Codable {
    let dataModel: [DataModel]
    let metadata: Metadata
    
    enum CodingKeys: String, CodingKey {
        case dataModel = "data"
        case metadata
    }
}

struct DataModel: Codable {
    let id: Int
    let name: String
    let registeredBy: RegisteredBy?
    let addresses: [Addresses]?
    let mailAddress: MailAddress?
    
    enum CodingKeys: String, CodingKey {
        case registeredBy = "regBy"
        case name
        case addresses
        case id
        case mailAddress
    }
}
struct Addresses: Codable {
    let type: String
    let address: String
    let zipcode: String
    let state: String
    let city: String
    let country: String
}
struct MailAddress: Codable {
    let address: String
    let city: String
    let zipcode: String
}
struct RegisteredBy: Codable {
    let name: String
}

struct Metadata: Codable {
    let total: Int
    let limit: Int
    let offset: Int
}
