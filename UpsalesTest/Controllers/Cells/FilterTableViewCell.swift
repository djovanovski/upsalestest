//
//  FilterTableViewCell.swift
//  UpsalesTest
//
//  Created by Darko Jovanovski on 10/30/18.
//  Copyright © 2018 Darko Jovanovski. All rights reserved.
//

import UIKit

class FilterTableViewCell: UITableViewCell {

    @IBOutlet weak var labelFilterTitle: UILabel!
    @IBOutlet weak var buttonCheckmark: UIButton!
    
    
    //MARK: - UITableViewCell overrides
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override var reuseIdentifier: String {
        return "FilterTableViewCell"
    }
    
    
    //MARK: - Custom functions
    func populateWith(managerUsers: ManagerUsers, selected: Bool) {
        self.labelFilterTitle.text = managerUsers.name
        let image = selected ? "" : ""
        let tintColor:UIColor = selected ? .brightBlue : .greyish
        self.buttonCheckmark.setTitle(image, for: .normal)
        self.buttonCheckmark.tintColor = tintColor

    }
    
    
}
