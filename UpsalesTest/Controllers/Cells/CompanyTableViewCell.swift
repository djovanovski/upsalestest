//
//  CompanyTableViewCell.swift
//  UpsalesTest
//
//  Created by Darko Jovanovski on 10/30/18.
//  Copyright © 2018 Darko Jovanovski. All rights reserved.
//

import UIKit

class CompanyTableViewCell: UITableViewCell {

    @IBOutlet weak var labelCompanyName: UILabel!
    @IBOutlet weak var labelManagerName: UILabel!

    
    //MARK: - UITableViewCell overrides
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override var reuseIdentifier: String {
        return "CompanyTableViewCell"
    }
    
    
    //MARK: - Custom functions
    func populateWith(dataModel: DataModel) {
        self.labelCompanyName.text = dataModel.name
        if let registeredBy = dataModel.registeredBy {
            self.labelManagerName.text = registeredBy.name
        }
        else {
            self.labelManagerName.text = ""
        }
    }
    
}
