//
//  AllFiltersTableViewCell.swift
//  UpsalesTest
//
//  Created by Darko Jovanovski on 10/30/18.
//  Copyright © 2018 Darko Jovanovski. All rights reserved.
//

import UIKit

class AllFiltersTableViewCell: UITableViewCell {

    @IBOutlet weak var labelFilterTitle: UILabel!
    @IBOutlet weak var labelFilterApplied: UILabel!
    
    
    //MARK: - UITableViewCell overrides
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override var reuseIdentifier: String {
        return "AllFiltersTableViewCell"
    }
    
    
    //MARK: - Custom functions
    func populateWith(filterModel: FiltersMock) {
        self.labelFilterTitle.text = filterModel.name
        self.labelFilterApplied.text = "Any"

    }
    
}
