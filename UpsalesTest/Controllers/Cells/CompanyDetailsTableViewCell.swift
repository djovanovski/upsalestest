//
//  CompanyDetailsTableViewCell.swift
//  UpsalesTest
//
//  Created by Darko Jovanovski on 10/31/18.
//  Copyright © 2018 Darko Jovanovski. All rights reserved.
//

import UIKit

class CompanyDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var labelCompanyName: UILabel!
    @IBOutlet weak var labelAddress: UILabel!
    @IBOutlet weak var labelCityName: UILabel!

    
    //MARK: - UITableViewCell overrides
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override var reuseIdentifier: String {
        return "CompanyDetailsTableViewCell"
    }
    
    
    //MARK: - Custom functions
    func populateWith(dataModel: Addresses) {
        self.labelCompanyName.text = dataModel.type + " address"
        self.labelAddress.text = dataModel.address + dataModel.zipcode
        self.labelCityName.text = dataModel.city
    }
    
}
