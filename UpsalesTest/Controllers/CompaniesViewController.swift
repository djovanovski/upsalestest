//
//  CompaniesViewController.swift
//  UpsalesTest
//
//  Created by Darko Jovanovski on 10/29/18.
//  Copyright © 2018 Darko Jovanovski. All rights reserved.
//

import UIKit

let HEADER_VIEW_HEIGHT:CGFloat = 76
private let LIMIT = 11
private let OFFSET = 10

class CompaniesViewController: UIViewController {
    
    
    /**
     - If i had more time I would make filtered and nonfiltered apiCall in one class so there wouldn't be so many DRY(don't repeat yourself)
     - Still many rooms for improvement but the time is limited.
     - Also there are a lot of strings/numbers/floats that need to go to some constants struct
     */
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var buttonFilter: UIButton!
    
    private var dataSource: [DataModel] = []
    private var metadata: Metadata?
    
    private var headerView: CompanyHeaderView!
    
    private var offsetCounter = 0
    private var filterEnabled: Bool = false
    
    //MARK: ViewController overrides
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.offsetCounter = 0
        if filterEnabled {
            self.buttonFilter.tintColor = .navy
            self.buttonFilter.backgroundColor = .white
        }
        else{
            self.buttonFilter.tintColor = .gray
            self.buttonFilter.backgroundColor = .clear
        }
    }
    
    
    //MARK: - Custom functions
    private func setup() {
        if !filterEnabled {
            self.apiCall()
        }
        self.setupTableView()
        self.offsetCounter += OFFSET
    }
    
    
    private func setupTableView() {
        self.tableView.register(UINib(nibName: CompanyTableViewCell().reuseIdentifier, bundle: nil), forCellReuseIdentifier: CompanyTableViewCell().reuseIdentifier)
        
        self.headerView = CompanyHeaderView(frame: CGRect(x: self.tableView.frame.origin.x,
                                                          y: self.tableView.frame.origin.y,
                                                          width: self.tableView.frame.width,
                                                          height: HEADER_VIEW_HEIGHT),
                                            title: "Companies",
                                            companiesNumber: "")
        
        self.tableView.tableHeaderView = self.headerView
        self.tableView.tableFooterView = UIView()
    }
    
    private func apiCall(append: Bool = false) {
        Networking().getAccounts(offset: offsetCounter, completion: { (result) in
            switch result {
            case .Success(let data):
                if append {
                    self.dataSource += data.dataModel
                }
                else {
                    self.dataSource = data.dataModel
                    
                }
                self.metadata = data.metadata
                DispatchQueue.main.async { [weak self] in
                    self?.tableView.reloadData()
                    self?.headerView.labelNumberOfCompanies.text = "\(self?.metadata?.total ?? 0) Companies"
                }
                
            case .Error(let error):
                DispatchQueue.main.async { 
                    showAlertWith(title: "Oops!", buttonTitle: "Ok", message: error, viewController: self)
                }
            }
        })
    }
    
    
    
    //MARK: - IBActions
    @IBAction func didTapOnFilter(_sender: UIButton) {
        if let navigationVc = self.storyboard?.instantiateViewController(withIdentifier: "navnav") as? UINavigationController, let root = navigationVc.viewControllers[0] as? AllFiltersViewController {
            root.allFiltersViewControllerDelegate = self
            self.navigationController?.present(navigationVc, animated: true, completion: nil)
            if filterEnabled {
                filterEnabled = false
            }
        }
        
    }
    
    
}

extension CompaniesViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CompanyTableViewCell().reuseIdentifier) as! CompanyTableViewCell
        
        cell.populateWith(dataModel: self.dataSource[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row == self.offsetCounter {
            self.offsetCounter += OFFSET
            //TODO: Make pagination for user.id - the same as companies
            if !filterEnabled {
                
                self.apiCall(append: true)
                
                
                //TODO: Make it custom with AwesomeFont if there is time left
                let spinner = UIActivityIndicatorView(style: .gray)
                spinner.startAnimating()
                spinner.frame = CGRect(x: self.tableView.frame.origin.x,
                                       y: self.tableView.frame.origin.y,
                                       width: self.tableView.bounds.width,
                                       height: self.tableView.rowHeight)
                
                
                self.tableView.tableFooterView = spinner
                self.tableView.tableFooterView?.isHidden = false
                
                if let total = self.metadata?.total, let limit = self.metadata?.limit {
                    if indexPath.row >= total - limit {
                        self.tableView.tableFooterView = UIView()
                        self.tableView.tableFooterView?.isHidden = true
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if filterEnabled {
            if let companyDetailsViewController = self.storyboard?.instantiateViewController(withIdentifier: "CompanyDetailsViewController") as? CompanyDetailsViewController {
                let item = self.dataSource[indexPath.row]
                
                companyDetailsViewController.dataSource = item
                self.navigationController?.pushViewController(companyDetailsViewController, animated: true)
            }
        }
        
    }
    
}

extension CompaniesViewController: AllFiltersViewControllerDelegate {
    func userHasCanceledFilter() {
        self.apiCall()
    }
    
    func filterCompany(userIds: String) {
        Networking().getAccounts(withUserIds: userIds) { (result) in
            switch result {
            case .Success(let data):
                self.dataSource = []
                self.dataSource = data.dataModel
                self.metadata = data.metadata
                self.filterEnabled = true
                
                DispatchQueue.main.async {
                    self.headerView.labelNumberOfCompanies.text = "\(data.metadata.total) Companies"
                    self.buttonFilter.tintColor = .navy
                    self.buttonFilter.backgroundColor = .white
                    self.tableView.reloadData()
                    self.tableView.tableFooterView = UIView()
                }
                
            case .Error(let error):
                DispatchQueue.main.async {
                    showAlertWith(title: "Oops!", buttonTitle: "Ok", message: error, viewController: self)
                }
            }
        }
    }
}
