//
//  FilterViewController.swift
//  UpsalesTest
//
//  Created by Darko Jovanovski on 10/30/18.
//  Copyright © 2018 Darko Jovanovski. All rights reserved.
//

import UIKit

private let FILTER_HEADER_VIEW_HEIGHT:CGFloat = 150

protocol FilterViewControllerDelegate: class {
    func didTapOnCancel()
    func didTapOnShowResults(userIds: String)
}

class FilterViewController: UIViewController {
    
    
    
    /**
     - Selected/not selecter are not in a different sections now.
     - Idealy I would put another DataSource where the selected would be in section 1 not selected in section 2
     - If there is no selected items just modify the dataSource so there would be 1 section only
     - SearchBar not implemented...
     */
    
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var selectedView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    
    private var dataSource: [ManagerUsers] = []
    private var selectedItems: [Int] = []
    private var selectedItemsString: String = ""
    
    
    
    weak var filterViewControllerDelegate: FilterViewControllerDelegate?
    
    //MARK: ViewController overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        Networking().getUsers { (result) in
            switch result {
            case .Success(let data):
                self.dataSource = data.data
                
                DispatchQueue.main.async { [weak self] in
                    self?.tableView.reloadData()
                }
                
            case .Error(let error):
                DispatchQueue.main.async {
                    showAlertWith(title: "Oops!", buttonTitle: "Ok", message: error, viewController: self)
                }
            }
        }
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .`default`
    }
    
    //MARK: - Custom functions
    private func setup() {
        self.setupTableView()
    }
    
    
    private func setupTableView() {
        self.tableView.register(UINib(nibName: FilterTableViewCell().reuseIdentifier, bundle: nil), forCellReuseIdentifier: FilterTableViewCell().reuseIdentifier)
        
        self.titleLabel.text = "Users"
        self.backButton.setTitle(" All filters", for: .normal)
        
        self.tableView.tableHeaderView = self.headerView
        self.tableView.tableFooterView = UIView()
    }
    
    private func createAppendFilter() {
        for id in self.selectedItems {
            self.selectedItemsString += "&user.id=\(id)"
        }
        self.filterViewControllerDelegate?.didTapOnShowResults(userIds: self.selectedItemsString)
        
    }
    
    //MARK: - IBActions
    @IBAction func didTapOnCancelButton(_sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
        self.filterViewControllerDelegate?.didTapOnCancel()
    }
    
    @IBAction func didTapOnShowResults(_sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
        self.createAppendFilter()
    }
    
    @IBAction func didTapBackButton(_sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension FilterViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FilterTableViewCell().reuseIdentifier) as! FilterTableViewCell
        let item = self.dataSource[indexPath.row]
        let isSelected = self.selectedItems.contains(item.id)
        cell.populateWith(managerUsers: item, selected: isSelected)
        
        return cell
    }
    
}

extension FilterViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.selectedItems.isEmpty {
            self.selectedItems.append(dataSource[indexPath.row].id)
        }
        else {
            if self.selectedItems.contains(dataSource[indexPath.row].id) {
                self.selectedItems = self.selectedItems.filter{$0 != dataSource[indexPath.row].id}
            }
            else {
                self.selectedItems.append(dataSource[indexPath.row].id)
            }
        }
        
        
        DispatchQueue.main.async { [weak self] in
            if let hasItems = self?.selectedItems.count, hasItems > 0 {
                self?.selectedView.isHidden = false
            }
            else {
                self?.selectedView.isHidden = true
            }
            self?.tableView.reloadRows(at: [indexPath], with: .none)
        }
        
    }
}

