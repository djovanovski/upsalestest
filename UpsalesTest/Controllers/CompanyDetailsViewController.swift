//
//  CompanyDetailsViewController.swift
//  UpsalesTest
//
//  Created by Darko Jovanovski on 10/31/18.
//  Copyright © 2018 Darko Jovanovski. All rights reserved.
//

import UIKit

class CompanyDetailsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var dataSource: DataModel?
    
    private var headerView: CompanyHeaderView!
    
    
    //MARK: ViewController overrides
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupBackButton()
    }

    
    //MARK: - Custom functions
    private func setup() {
        self.setupTableView()
    }
    
    
    private func setupTableView() {
        self.tableView.register(UINib(nibName: CompanyDetailsTableViewCell().reuseIdentifier, bundle: nil), forCellReuseIdentifier: CompanyDetailsTableViewCell().reuseIdentifier)
        
        self.headerView = CompanyHeaderView(frame: CGRect(x: self.tableView.frame.origin.x,
                                                          y: self.tableView.frame.origin.y,
                                                          width: self.tableView.frame.width,
                                                          height: HEADER_VIEW_HEIGHT),
                                            title: self.dataSource?.name ?? "",
                                            companiesNumber: self.dataSource?.addresses?[0].city ?? "")
        
        self.tableView.tableHeaderView = self.headerView
        self.tableView.tableFooterView = UIView()
    }
    private func setupBackButton() {
        self.navigationItem.rightBarButtonItem = nil
        
        let button = UIButton(type: .custom)
        
        button.titleLabel?.font = UIFont(name: "FontAwesome", size: 20)
        button.setTitle("", for: .normal)
        button.tintColor = .white
        //TODO: fix this with some kind of constants
        button.addTarget(self, action: #selector(fbButtonPressed), for: .touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 53, height: 51)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: -40, bottom: 0, right: 0)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    @objc func fbButtonPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}

extension CompanyDetailsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource?.addresses?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CompanyDetailsTableViewCell().reuseIdentifier) as! CompanyDetailsTableViewCell
        
        if let item = self.dataSource, let addresses = item.addresses?[indexPath.row]{
            cell.populateWith(dataModel: addresses)
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let mapsViewController = self.storyboard?.instantiateViewController(withIdentifier: "MapsViewController") as? MapsViewController {
            self.navigationController?.present(mapsViewController, animated: true, completion: nil)
        }
        
    }
}

