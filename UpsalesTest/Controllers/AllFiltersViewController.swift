//
//  AllFiltersViewController.swift
//  UpsalesTest
//
//  Created by Darko Jovanovski on 10/30/18.
//  Copyright © 2018 Darko Jovanovski. All rights reserved.
//

import UIKit

protocol AllFiltersViewControllerDelegate: class {
    func filterCompany(userIds: String)
    func userHasCanceledFilter()

}

class AllFiltersViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var dataSource: [FiltersMock] = [FiltersMock(name: "Account manager")]

    weak var allFiltersViewControllerDelegate: AllFiltersViewControllerDelegate?
    
    //MARK: ViewController overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .`default`
    }
    
    //MARK: - Custom functions
    private func setup() {
        self.setupTableView()
    }
    
    
    private func setupTableView() {
        self.tableView.register(UINib(nibName: AllFiltersTableViewCell().reuseIdentifier, bundle: nil), forCellReuseIdentifier: AllFiltersTableViewCell().reuseIdentifier)
        self.tableView.tableFooterView = UIView()
    }
    
    private func canceledFilter() {
        self.dismiss(animated: true, completion: nil)
        self.allFiltersViewControllerDelegate?.userHasCanceledFilter()
    }
    
    //MARK: - IBActions
    @IBAction func didTapOnCancel(_sender: UIButton) {
        self.canceledFilter()
    }
    

}

extension AllFiltersViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AllFiltersTableViewCell().reuseIdentifier) as! AllFiltersTableViewCell
        cell.populateWith(filterModel: self.dataSource[indexPath.row])
        
        return cell
    }
    
    
}

extension AllFiltersViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if let filterViewController = self.storyboard?.instantiateViewController(withIdentifier: "FilterViewController") as? FilterViewController {
            filterViewController.filterViewControllerDelegate = self
            self.navigationController?.pushViewController(filterViewController, animated: true)
        }
    }
}

extension AllFiltersViewController: FilterViewControllerDelegate {
    func didTapOnShowResults(userIds: String) {
        self.dismiss(animated: true) {
            self.allFiltersViewControllerDelegate?.filterCompany(userIds: userIds)
        }
    }
    
    func didTapOnCancel() {
        self.canceledFilter()
    }

}

