//
//  Helpers.swift
//  UpsalesTest
//
//  Created by Darko Jovanovski on 10/31/18.
//  Copyright © 2018 Darko Jovanovski. All rights reserved.
//

import UIKit


func showAlertWith(title: String, buttonTitle: String, message: String, style: UIAlertController.Style = .alert, viewController: UIViewController) {
    
    let alertController = UIAlertController(title: title, message: message, preferredStyle: style)
    let action = UIAlertAction(title: buttonTitle, style: .default) { (action) in
        viewController.dismiss(animated: true, completion: nil)
    }
    alertController.addAction(action)
    viewController.present(alertController, animated: true, completion: nil)
}
